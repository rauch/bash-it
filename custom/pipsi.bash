PIPSI_BIN_DIR="$HOME/.local/bin"

if [ -d "$PIPSI_BIN_DIR" ]; then
  export PATH="$PIPSI_BIN_DIR:$PATH"
fi
